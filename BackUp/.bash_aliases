export storage="/media/dylansavoia/Storage"

goto() {
	res=$(find ../ -type d -name "*$1*" | head -1)
	if [ "$res" = "" ]; then
		echo "No Folder Found"
	else
		cd $res
	fi
}

hfind() {
	history | grep "$1"
	last=`history | grep $1 | tail -2 | head -1 | grep -oh "[A-Za-z~\/\.].*$"`
	history -s $last
}

search() {
	python3 /home/dylansavoia/Scripts/quick_search.py "$1" "$2"
}
